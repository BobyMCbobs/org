

-- Prepare to migrate schema to public

DO
$$
DECLARE
    row record;
BEGIN
    FOR row IN SELECT tablename FROM pg_tables WHERE schemaname = 'nextcloud'
    LOOP
        EXECUTE 'ALTER TABLE nextcloud.' || quote_ident(row.tablename) || ' SET SCHEMA public;';
    END LOOP;
END;
$$;
