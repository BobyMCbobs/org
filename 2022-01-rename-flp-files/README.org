#+TITLE: Rename FLP files

* Issue
The naming scheme for my FL Studio project files is annoying, so I want to migrate to using the format of 'YYYY-mm-dd NOTE.flp'

* Creating a script
#+begin_src shell
PROJECT_FOLDER="$HOME/Documents/Image-Line/FL Studio/Projects"
# match the way I've been writing file names: ddmmyy something 1!.flp
re="^([0-9]+|)([a-zA-Z! _'.0-9\(\)#-]+|)([0-9]|).flp$"
# match the date
baddatere="^([0-9][0-9])([0-9][0-9])([0-9][0-9])$"
cd "${PROJECT_FOLDER}"
for i in *; do
    # no diretories
    if [ -d "${i}" ]; then
        continue
    fi
    if [[ $i =~ $re ]]; then
        # extract file date and file note from file name
        filedate=${BASH_REMATCH[1]}
        filenote=${BASH_REMATCH[2]}
        # remoce space from start of filenote
        if [[ "${filenote:0:1}" = " " ]]; then
            filenote="${filenote:1}"
        fi
        # if the extracted date is longer than 6 characters (not ddmmyy)
        if [[ "${#filedate}" -gt 6 ]]; then
            filedate="$(stat -c '%y' "${i}")"
            newdate="$(date -d "${filedate}" +%Y-%m-%d)"
        # else if it matches the date format of ddmmyy
        elif [[ $filedate =~ $baddatere ]]; then
            day=${BASH_REMATCH[1]}
            month=${BASH_REMATCH[2]}
            year=${BASH_REMATCH[3]}
            newdate="20${year}-${month}-${day}"
        fi
        newfilename="${newdate}"
        # append the filenote if it's not empty
        if [ -n "${filenote}" ]; then
            newfilename="${newfilename} ${filenote}"
        fi
        # rename
        echo "'${filedate}${filenote}.flp' => '${newfilename}.flp'"
        mv "${i}" "${newfilename}.flp"
    else
        echo "UNABLE TO PARSE '${i}'"
    fi
done
#+end_src
