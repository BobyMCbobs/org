#+TITLE: CockroachDB and Go-Migrate

* What are they
- CockroachDB :: is a distributed document database with a Postgres interface
- go-migrate :: is a SQL migration tool and package in Go

* Start a CockroachDB instance
#+begin_src tmate :window dbfun
podman run \
    -it \
   --rm \
   --name=cockroachdb \
   -p 26257:26257 -p 8080:8080  \
   cockroachdb/cockroach:v21.2.5 start-single-node \
   --insecure
#+end_src

* Declare some migrations
#+begin_src shell
mkdir -p ./migrations/
#+end_src

#+begin_src sql :tangle ./migrations/20200315101517_users.up.sql
-- flattrack.users definition

begin;

create table if not exists users (
  id text default (md5(random()::text || clock_timestamp()::text)::uuid)::text not null,
  names text not null,
  email text not null,
  password text,
  phoneNumber text,
  birthday int,
  contractAgreement bool default false not null,
  disabled bool default false not null,
  registered bool default false not null,
  lastLogin int not null default 0,
  authNonce text default (md5(random()::text || clock_timestamp()::text)::uuid)::text not null,
  creationTimestamp int not null default extract('epoch' from now())::int,
  modificationTimestamp int not null default extract('epoch' from now())::int,
  deletionTimestamp int not null default 0,

  primary key (id)
);

comment on table users is 'The users table is used for storing user accounts';

commit;
#+end_src

#+begin_src sql :tangle ./migrations/20200315101517_users.down.sql
begin;

drop table if exists users cascade;

commit;
#+end_src

* Gogoroachrangers!
#+begin_src shell :results silent
go mod init gogoroachrangers
#+end_src

#+begin_src go :tangle ./main.go
package main

import (
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/cockroachdb"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func main() {
	uri :="cockroach://root:@localhost:26257/defaultdb?sslmode=disable"
  c := &cockroachdb.CockroachDb{}
  d, err := c.Open(uri)
  if err != nil {
		fmt.Println(err)
		return
  }
	m, err := migrate.NewWithDatabaseInstance("file://./migrations", "migrate", d)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = m.Up()
	if err != nil {
		fmt.Println(err)
		return
	}
}
#+end_src

#+begin_src tmate :window dbfun-run
go mod tidy
#+end_src

#+begin_src tmate :window dbfun-run
go run ./main.go
#+end_src

* CockroachDB and Postgres
It seems like the two don't have exact compatibility.

for example the type uuid doesn't go into text, so it had to be wrapped in text.
#+begin_src sql
  id text default (md5(random()::text || clock_timestamp()::text)::uuid)::text not null,
#+end_src

and
#+begin_src sql
date_part('epoch',CURRENT_TIMESTAMP)::int
#+end_src
had to become
#+begin_src sql
extract('epoch' from now())::int
#+end_src
which actually seems cleaner

* Notes
- https://github.com/golang-migrate/migrate/blob/586cc866f5ece6a5624ecebb733f1fab0e63af7e/database/cockroachdb/cockroachdb_test.go
- https://github.com/golang-migrate/migrate/blob/586cc866f5ece6a5624ecebb733f1fab0e63af7e/database/cockroachdb/TUTORIAL.md
- https://github.com/golang-migrate/migrate/blob/586cc866f5ece6a5624ecebb733f1fab0e63af7e/database/cockroachdb/cockroachdb.go
- https://github.com/cockroachdb/cockroach-go
