package main

import (
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/cockroachdb"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func main() {
	uri :="cockroach://root:@localhost:26257/defaultdb?sslmode=disable"
  c := &cockroachdb.CockroachDb{}
  d, err := c.Open(uri)
  if err != nil {
		fmt.Println(err)
		return
  }
	m, err := migrate.NewWithDatabaseInstance("file://./migrations", "migrate", d)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = m.Up()
	if err != nil {
		fmt.Println(err)
		return
	}
}
