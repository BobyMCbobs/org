

// #+RESULTS:
// #+begin_example
// poolsize setting default to 3
// environment 'nodejs' created
// #+end_example

// Example code

module.exports = async function(context) {
    return {
        status: 200,
        body: "hello, world!\n"
    };
}
