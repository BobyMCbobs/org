// A test application

// Here's the code, it's a very basic webserver:

package main

import (
    "fmt"
    "net/http"
)

func main() {
    http.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, "Hello World from knative and ko!")
    })

    http.ListenAndServe(":8080", nil)
}
