#+TITLE: ko+kn

* Introduction
- ko :: ko is a container builder for Go applications
- kn :: kn is the CLI tool for Knative

These tools can be used together to make quick application build and deploys

* A test application
Here's the code, it's a very basic webserver:
#+begin_src go :tangle ./main.go
package main

import (
    "fmt"
    "net/http"
)

func main() {
    http.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, "Hello World from knative and ko!")
    })

    http.ListenAndServe(":8080", nil)
}
#+end_src

Set up the Go workspace
#+begin_src shell :results silent
go mod init gitlab.com/bobymcbobs/container-images/hello-world-go-http
#+end_src

* Bringing up the application
#+begin_src shell
export KO_DOCKER_REPO=registry.gitlab.com/bobymcbobs/container-images/hello-world-go-http
kn service create hello-world-go-http --image=$(ko build --platform all .)
#+end_src

#+begin_src shell
kn service describe hello-world-go-http
#+end_src

#+RESULTS:
#+begin_example
Name:       hello-world-go-http
Namespace:  default
Age:        6m
URL:        https://hello-world-go-http.default.home-network.islive.xyz

Revisions:
  100%  @latest (hello-world-go-http-00001) [1] (6m)
        Image:     registry.gitlab.com/bobymcbobs/container-images/hello-world-go-http/hello-world-go-http-7ef7fe06a23a329c58ffca1414c3ace6@sha256:8020f87540e2f6876fb2f6ecddb917276ac043433ac90239fa6f92cd762d10e2 (at 8020f8)
        Replicas:  1/1

Conditions:
  OK TYPE                   AGE REASON
  ++ Ready                   5m
  ++ ConfigurationsReady     6m
  ++ RoutesReady             5m
#+end_example

* TODO Verify the SBOM with cosign
#+begin_src shell
CONTAINER_IMAGE=$(kn service describe hello-world-go-http -o=jsonpath='{.spec.template.spec.containers[0].image}')
cosign download sbom "${CONTAINER_IMAGE}"
#+end_src

#+RESULTS:
#+begin_example
#+end_example

* Updating the to a new version
#+begin_src shell
export KO_DOCKER_REPO=registry.gitlab.com/bobymcbobs/container-images/hello-world-go-http
kn service update hello-world-go-http --image=$(ko build --platform all .)
#+end_src

* Deleting the application
#+begin_src shell
kn service delete hello-world-go-http
#+end_src

#+RESULTS:
#+begin_example
Service 'hello-world-go-http' successfully deleted in namespace 'default'.
#+end_example
